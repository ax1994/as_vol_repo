import pandas as pd 
import matplotlib.pyplot as plt 
import numpy as np
from scipy.stats import (shapiro,
                         normaltest,
                         kurtosistest,
                         anderson)
from sklearn.metrics import mean_squared_error
from datetime import datetime as dt
import pickle
from sklearn.model_selection import (RepeatedKFold,
                                     GridSearchCV)
from sklearn.metrics import (accuracy_score,
                             f1_score,
                             recall_score,
                             precision_score)
from keras.models import Sequential
from keras.layers import Dense
from keras import backend as K
from statsmodels.tsa.arima_process import ArmaProcess
from statsmodels.tsa.stattools import adfuller
import statsmodels.api as sm
import datetime
import math
from matplotlib import rcParams


class Colour:
    PURPLE = '\033[95m'
    CYAN = '\033[96m'
    DARKCYAN = '\033[36m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    END = '\033[0m'


def check_duplicates(data, cols):
    """
    """
    duplicates = None
    try:
        duplicates = data[data.duplicated(subset=cols, keep=False)].copy()
        duplicates.sort_values(cols, inplace=True)

        if duplicates.empty:
            print("CHECK DUPLICATES PASS: No Duplicated Rows Found For Columns {}".format(
                cols)
            )
        else:
            print("CHECK DUPLICATES FAIL: {} Duplicated Rows Found For Columns {}".format(
                duplicates.shape[0], cols)
            )
    except KeyError:
        print('{} Columns Not Found In Dataset'.format(cols))
    return duplicates


def count_missing(data, cols, sigfig=4):
    print(
        *["{} missing: {} ({}%) values".format(col,
                                               data[col].isna().sum(),
                                               round(data[col].isna().sum() / data.shape[0],
                                                     sigfig) * 100) for col in cols
          ],
        sep="\n"
    )
    return


def calculate_profit_ratio(odds_string, sigfig=2):
    if not isinstance(odds_string, str):
        return odds_string
    else:
        try:
            winnings, stake = odds_string.split("/")[0], odds_string.split("/")[1]
            return round(int(winnings) / int(stake), sigfig)
        except ValueError:
            return np.nan


def today_string():
    return dt.today().strftime('%Y_%m_%d')


def run_grid_search_with_cv(model,
                            params,
                            X,
                            y,
                            scoring='f1',
                            n_splits=10,
                            n_repeats=3,
                            random_state=1):
    cv = RepeatedKFold(n_splits=n_splits,
                       n_repeats=n_repeats,
                       random_state=random_state)

    search = GridSearchCV(model,
                          params,
                          scoring=scoring,
                          n_jobs=-1,
                          cv=cv)
    result = search.fit(X, y)

    print('Best Score: %s' % result.best_score_)
    print('Best Hyper-parameters: %s' % result.best_params_)
    return result


def calc_model_metrics(y_pred,
                       y_test,
                       sigfig=3):
    acc = round(accuracy_score(y_pred, y_test), sigfig)
    f1 = round(f1_score(y_pred, y_test), sigfig)
    recall = round(recall_score(y_pred, y_test), sigfig)
    precision = round(precision_score(y_pred, y_test), sigfig)
    return acc, f1, recall, precision


def eval_model(model,
               X_train,
               y_train,
               X_test,
               y_test,
               sigfig=3):
    model.fit(X_train, y_train)
    y_pred = model.predict(X_test)
    acc, f1, recall, precision = calc_model_metrics(y_pred,
                                                    y_test,
                                                    sigfig)
    print("Accuracy: {} \nF1 Score: {}  \nPrecision Score: {}  \nRecall Score: {}".format(
        acc,
        f1,
        recall,
        precision)
    )

    results = {
        "Model": model.__class__.__name__,
        "Accuracy": acc,
        "F1": f1,
        "Recall": recall,
        "Precision": precision
    }

    return model, results


def pickle_object(obj, path):
    with open(path, 'wb') as handle:
        pickle.dump(obj,
                    handle,
                    protocol=pickle.HIGHEST_PROTOCOL)


def unpickle_object(path):
    with open(path, 'rb') as handle:
        b = pickle.load(handle)
        return b


# generate a simple 2HL layer NN with a sigmoid activation layer
def get_neural_network(optimizer='adam'):
    model = Sequential()
    model.add(Dense(150, input_dim=28, activation='relu'))
    model.add(Dense(80, activation='relu'))
    model.add(Dense(1, activation='sigmoid'))
    model.compile(loss='binary_crossentropy',
                  optimizer=optimizer,
                  metrics=['accuracy'])
    return model


def recall_m(y_true, y_pred):
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
    recall = true_positives / (possible_positives + K.epsilon())
    return recall


def precision_m(y_true, y_pred):
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
    precision = true_positives / (predicted_positives + K.epsilon())
    return precision


def f1_m(y_true, y_pred):
    precision = precision_m(y_true, y_pred)
    recall = recall_m(y_true, y_pred)
    return 2*((precision*recall)/(precision+recall+K.epsilon()))


def calc_rmse(y_true,
              y_pred):
    return np.sqrt(mean_squared_error(y_true, y_pred))


def simulate_ar1_process(theta,
                         base=0):
    ar1 = np.array([1, -theta])

    # No MA process (ARIMA(1,1) model simulator but with MA(0) it becomes AR(1) Sim)
    ma1 = np.array([1])

    ar_object1 = ArmaProcess(ar1, ma1)
    return ar_object1.generate_sample(nsample=100) + base


def volatility_model_plot(returns_true,
                          vol_true,
                          returns_pred,
                          vol_pred,
                          time_index,
                          vol_model_name=''
                          ):
    if type(vol_pred) != pd.DataFrame:
        vol_pred = pd.DataFrame(vol_pred)

    rcParams.update({'font.size': 7})

    plt.figure(figsize=(10, 8))
    plt.plot(time_index, vol_true, label='SP500 Realized Volatility')
    plt.plot(time_index, vol_pred, label='SP500 {} Volatility Prediction'.format(vol_model_name))
    plt.ylim(0)
    plt.title('Out-Sample {} Conditional Vs Realized Volatility'.format(vol_model_name), fontsize=12)
    plt.legend()
    plt.show()

    fig, ax = plt.subplots(nrows=2,
                           ncols=2,
                           tight_layout=True)

    vol_lower_bound = (returns_pred - vol_pred).iloc[:, 0].values
    vol_upper_bound = (returns_pred + vol_pred).iloc[:, 0].values

    ax[0, 0].plot(time_index, returns_true, color='black', label='Log Returns')
    ax[0, 0].fill_between(time_index,
                          vol_lower_bound,
                          vol_upper_bound,
                          color='lightblue',
                          alpha=1,
                          label='ARMA Predicted ± Volatility')
    ax[0, 0].set_xlim([datetime.date(2016, 1, 1), datetime.date(2016, 12, 31)])
    ax[0, 0].set_ylim(-4, 4)
    ax[0, 0].tick_params(labelrotation=45)
    ax[0, 0].title.set_text('2014 ARMA(1,1) Adjusted for ± {} Predicted Volatility'.format(vol_model_name))
    ax[0, 0].legend(loc='upper left')

    ax[1, 0].plot(time_index, returns_true, color='black', label='Log Returns')
    ax[1, 0].fill_between(time_index,
                          vol_lower_bound,
                          vol_upper_bound,
                          color='lightblue',
                          alpha=1,
                          label='ARMA Predicted ± Volatility')
    ax[1, 0].set_xlim([datetime.date(2018, 1, 1), datetime.date(2018, 12, 31)])
    ax[1, 0].set_ylim(-4, 4)
    ax[1, 0].tick_params(labelrotation=45)
    ax[1, 0].title.set_text('2016 ARMA(1,1) Adjusted for ± {} Predicted Volatility'.format(vol_model_name))
    ax[1, 0].legend(loc='upper left')

    ax[0, 1].plot(time_index, returns_true, color='black', label='Log Returns')
    ax[0, 1].fill_between(time_index,
                          vol_lower_bound,
                          vol_upper_bound,
                          color='lightblue',
                          alpha=1,
                          label='ARMA Predicted ± Volatility')
    ax[0, 1].set_xlim([datetime.date(2017, 1, 1), datetime.date(2017, 12, 31)])
    ax[0, 1].set_ylim(-4, 4)
    ax[0, 1].tick_params(labelrotation=45)
    ax[0, 1].title.set_text('2015 ARMA(1,1) Adjusted for ± {} Predicted Volatility'.format(vol_model_name))
    ax[0, 1].legend(loc='upper left')

    ax[1, 1].plot(time_index, returns_true, color='black', label='Log Returns')
    ax[1, 1].fill_between(time_index,
                          vol_lower_bound,
                          vol_upper_bound,
                          color='lightblue',
                          alpha=1,
                          label='ARMA Predicted ± Volatility')
    ax[1, 1].set_xlim([datetime.date(2019, 1, 1), datetime.date(2019, 12, 31)])
    ax[1, 1].set_ylim(-4, 4)
    ax[1, 1].tick_params(labelrotation=45)
    ax[1, 1].title.set_text('2017 ARMA(1,1) Adjusted for ± {} Predicted Volatility'.format(vol_model_name))
    ax[1, 1].legend(loc='upper left')

    plt.show()


def generate_full_stats(df,
                        col,
                        sigfig=3,
                        stats=None):
    if stats is None:
        stats = ['skew',
                 'kurtosis']

    pandas_results = df[col].describe().to_frame().reset_index()
    pandas_results.columns = ['Stat', 'Value']

    additional_results = [{"Stat": stat,
                           "Value": getattr(df[col], stat)()}
                          for stat
                          in stats]

    full_stats = pandas_results.append(additional_results,
                                       ignore_index=True)

    full_stats.replace(["count", "25%", "50%", "75%"],
                       ["n", "Q-25%", "median", "Q-75%"],
                       inplace=True)

    full_stats['Stat'] = full_stats.Stat.str.title()
    full_stats.round(sigfig)

    return full_stats


# Get Anderson-Darling test to return p-value rather than CI levels.
def anderson_darling_test(data):
    ad, crit, sig = anderson(data, dist='norm')
    ad = ad * (1 + (.75 / 50) + 2.25 / (50 ** 2))
    if ad >= .6:
        p = math.exp(1.2937 - 5.709 * ad - .0186 * (ad ** 2))
    elif ad >= .34:
        p = math.exp(.9177 - 4.279 * ad - 1.38 * (ad ** 2))
    elif ad > .2:
        p = 1 - math.exp(-8.318 + 42.796 * ad - 59.938 * (ad ** 2))
    else:
        p = 1 - math.exp(-13.436 + 101.14 * ad - 223.73 * (ad ** 2))
    return ad, p


sample_stat_test_table = []


def run_sample_stat_tests(data, col, sigfig=3):
    # ADP applied to log returns and residual
    adf_test_data = adfuller(data[col].fillna(0))

    adjusted_df_result = {
        "Test": "Adjusted-DF",
        "Stat": adf_test_data[0],
        "P": adf_test_data[1],
        "Null": "Series contains Unit Root"
    }

    ljung_box = sm.stats.acorr_ljungbox(data[col].values,
                                        lags=20,
                                        return_df=True)

    ljung_box_result = {
        "Test": "Ljung-Box-20",
        "Stat": ljung_box.lb_stat.iloc[0],
        "P": ljung_box.lb_pvalue.iloc[0],
        "Null": "Series is White Noise"
    }

    # Shapiro Wilks
    w, p = shapiro(data[col])
    shapiro_wilks_result = {
        "Test": "Shapiro-Wilks",
        "Stat": w,
        "P": p,
        "Null": "Series is Normal Distribution"
    }

    # DAgostino k-squared test
    w, p = normaltest(data[col])

    dk2_result = {
        "Test": "D'Agostino-K-squared",
        "Stat": w,
        "P": p,
        "Null": "Series contains Normal Kurtosis and Skew"
    }

    # Anscombe-Glynn Kurtosis Test
    w, p = kurtosistest(data[col])
    anscombe_glynn_result = {
        "Test": "Anscombe-Glynn",
        "Stat": w,
        "P": p,
        "Null": "True Kurtosis is 3"
    }

    sample_stats_table = pd.DataFrame([adjusted_df_result,
                                       ljung_box_result,
                                       shapiro_wilks_result,
                                       dk2_result,
                                       anscombe_glynn_result])

    return sample_stats_table


# Temp function until I figure out a way to vectorise above and run all in one big function
def process_arch_results(results_list):
    results_df = pd.DataFrame(results_list)

    # save the old index as the ModelRunId (order in which models run) before sorting
    results_df['ModelID'] = results_df.index
    results_df.sort_values('BIC', ascending=True, inplace=True)

    # Create a new index with the sorted order (rank) of the models
    results_df['ModelRank'] = results_df.reset_index().index
    results_df.set_index("ModelRank", inplace=True)

    # Put model ID first in column order
    results_df = results_df[["ModelID"] + [col for col in results_df.columns if col != "ModelID"]]
    results_df = results_df.round({'AIC': 1, 'BIC': 1, "LogL": 1})

    return results_df
